from django.db import models

# Create your models here.

from cms.models.pluginmodel import CMSPlugin
from tinymce.models import HTMLField
class Poem(CMSPlugin):
    title = models.CharField(max_length=50, default='Title')
    #body = models.CharField(max_length=1000, default='MyPoem')
    body = HTMLField()