from django.apps import AppConfig


class AnasConfig(AppConfig):
    default_auto_field = 'django.db.models.BigAutoField'
    name = 'anas'
